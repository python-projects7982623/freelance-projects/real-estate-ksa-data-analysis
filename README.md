```python
import pandas as pd
```

## Constants


```python
dataPath = 'REALESTATE/SA_Aqar.csv'
```

# Reading Data


```python
df = pd.read_csv(dataPath)
```


```python
len(df.columns)
```




    24




```python
df.info()
```

    <class 'pandas.core.frame.DataFrame'>
    RangeIndex: 3718 entries, 0 to 3717
    Data columns (total 24 columns):
     #   Column        Non-Null Count  Dtype 
    ---  ------        --------------  ----- 
     0   city          3718 non-null   object
     1   district      3718 non-null   object
     2   front         3718 non-null   object
     3   size          3718 non-null   int64 
     4   property_age  3718 non-null   int64 
     5   bedrooms      3718 non-null   int64 
     6   bathrooms     3718 non-null   int64 
     7   livingrooms   3718 non-null   int64 
     8   kitchen       3718 non-null   int64 
     9   garage        3718 non-null   int64 
     10  driver_room   3718 non-null   int64 
     11  maid_room     3718 non-null   int64 
     12  furnished     3718 non-null   int64 
     13  ac            3718 non-null   int64 
     14  roof          3718 non-null   int64 
     15  pool          3718 non-null   int64 
     16  frontyard     3718 non-null   int64 
     17  basement      3718 non-null   int64 
     18  duplex        3718 non-null   int64 
     19  stairs        3718 non-null   int64 
     20  elevator      3718 non-null   int64 
     21  fireplace     3718 non-null   int64 
     22  price         3718 non-null   int64 
     23  details       3638 non-null   object
    dtypes: int64(20), object(4)
    memory usage: 697.2+ KB
    


```python
df.head(5)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city</th>
      <th>district</th>
      <th>front</th>
      <th>size</th>
      <th>property_age</th>
      <th>bedrooms</th>
      <th>bathrooms</th>
      <th>livingrooms</th>
      <th>kitchen</th>
      <th>garage</th>
      <th>...</th>
      <th>roof</th>
      <th>pool</th>
      <th>frontyard</th>
      <th>basement</th>
      <th>duplex</th>
      <th>stairs</th>
      <th>elevator</th>
      <th>fireplace</th>
      <th>price</th>
      <th>details</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>الرياض</td>
      <td>حي العارض</td>
      <td>شمال</td>
      <td>250</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>80000</td>
      <td>للايجار فيلا دبلكس في موقع ممتاز جدا بالقرب من...</td>
    </tr>
    <tr>
      <th>1</th>
      <td>الرياض</td>
      <td>حي القادسية</td>
      <td>جنوب</td>
      <td>370</td>
      <td>0</td>
      <td>4</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>60000</td>
      <td>*** فيلا درج مع الصالة جديدة ***\n\nعبارة عن م...</td>
    </tr>
    <tr>
      <th>2</th>
      <td>الرياض</td>
      <td>حي القادسية</td>
      <td>جنوب</td>
      <td>380</td>
      <td>0</td>
      <td>4</td>
      <td>5</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>60000</td>
      <td>فيلا للايجار درج داخلي مشب خارجي مجلس مقلط وصا...</td>
    </tr>
    <tr>
      <th>3</th>
      <td>الرياض</td>
      <td>حي المعيزلة</td>
      <td>غرب</td>
      <td>250</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>3</td>
      <td>0</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>55000</td>
      <td>فيلا للايجار        جديده لن تستخدم          ش...</td>
    </tr>
    <tr>
      <th>4</th>
      <td>الرياض</td>
      <td>حي العليا</td>
      <td>غرب</td>
      <td>400</td>
      <td>11</td>
      <td>7</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>70000</td>
      <td>فيلا للايجار حي العليا \n\nالارضي مجالس وغرفتي...</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 24 columns</p>
</div>




```python
df.describe()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>size</th>
      <th>property_age</th>
      <th>bedrooms</th>
      <th>bathrooms</th>
      <th>livingrooms</th>
      <th>kitchen</th>
      <th>garage</th>
      <th>driver_room</th>
      <th>maid_room</th>
      <th>furnished</th>
      <th>ac</th>
      <th>roof</th>
      <th>pool</th>
      <th>frontyard</th>
      <th>basement</th>
      <th>duplex</th>
      <th>stairs</th>
      <th>elevator</th>
      <th>fireplace</th>
      <th>price</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3718.000000</td>
      <td>3.718000e+03</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>390.968531</td>
      <td>5.064820</td>
      <td>5.083916</td>
      <td>4.606509</td>
      <td>2.243948</td>
      <td>0.909360</td>
      <td>0.802044</td>
      <td>0.495697</td>
      <td>0.795320</td>
      <td>0.123453</td>
      <td>0.560785</td>
      <td>0.521517</td>
      <td>0.162453</td>
      <td>0.802582</td>
      <td>0.034158</td>
      <td>0.499462</td>
      <td>0.814416</td>
      <td>0.080958</td>
      <td>0.181280</td>
      <td>8.738797e+04</td>
    </tr>
    <tr>
      <th>std</th>
      <td>1565.056135</td>
      <td>7.590427</td>
      <td>1.230040</td>
      <td>0.703449</td>
      <td>0.916436</td>
      <td>0.287135</td>
      <td>0.398512</td>
      <td>0.500049</td>
      <td>0.403522</td>
      <td>0.329001</td>
      <td>0.496358</td>
      <td>0.499604</td>
      <td>0.368915</td>
      <td>0.398104</td>
      <td>0.181660</td>
      <td>0.500067</td>
      <td>0.388823</td>
      <td>0.272807</td>
      <td>0.385302</td>
      <td>7.063470e+04</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000e+03</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>280.000000</td>
      <td>0.000000</td>
      <td>4.000000</td>
      <td>4.000000</td>
      <td>2.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>5.500000e+04</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>330.000000</td>
      <td>2.000000</td>
      <td>5.000000</td>
      <td>5.000000</td>
      <td>2.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>7.000000e+04</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>400.000000</td>
      <td>7.000000</td>
      <td>6.000000</td>
      <td>5.000000</td>
      <td>3.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>1.000000e+05</td>
    </tr>
    <tr>
      <th>max</th>
      <td>95000.000000</td>
      <td>36.000000</td>
      <td>7.000000</td>
      <td>5.000000</td>
      <td>5.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.000000</td>
      <td>1.700000e+06</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.isnull().sum()
```




    city             0
    district         0
    front            0
    size             0
    property_age     0
    bedrooms         0
    bathrooms        0
    livingrooms      0
    kitchen          0
    garage           0
    driver_room      0
    maid_room        0
    furnished        0
    ac               0
    roof             0
    pool             0
    frontyard        0
    basement         0
    duplex           0
    stairs           0
    elevator         0
    fireplace        0
    price            0
    details         80
    dtype: int64



# Data Preparation


```python
df.sort_values(by='price',ascending=False, inplace=True)
```


```python
df['details'].isna().sum()
```




    80




```python
df[df['details'].isna()]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>city</th>
      <th>district</th>
      <th>front</th>
      <th>size</th>
      <th>property_age</th>
      <th>bedrooms</th>
      <th>bathrooms</th>
      <th>livingrooms</th>
      <th>kitchen</th>
      <th>garage</th>
      <th>...</th>
      <th>roof</th>
      <th>pool</th>
      <th>frontyard</th>
      <th>basement</th>
      <th>duplex</th>
      <th>stairs</th>
      <th>elevator</th>
      <th>fireplace</th>
      <th>price</th>
      <th>details</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>168</th>
      <td>الرياض</td>
      <td>حي الملك عبدالله</td>
      <td>جنوب</td>
      <td>2500</td>
      <td>10</td>
      <td>6</td>
      <td>5</td>
      <td>3</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>800000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>844</th>
      <td>الرياض</td>
      <td>حي الخزامى</td>
      <td>جنوب</td>
      <td>400</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>330000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>544</th>
      <td>الرياض</td>
      <td>حي الرائد</td>
      <td>غرب</td>
      <td>350</td>
      <td>0</td>
      <td>6</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>300000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1570</th>
      <td>جدة</td>
      <td>حي الشاطئ</td>
      <td>شمال</td>
      <td>300</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>250000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1825</th>
      <td>جدة</td>
      <td>حي الشاطئ</td>
      <td>شمال</td>
      <td>300</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>250000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>1956</th>
      <td>الدمام</td>
      <td>حي ضاحية الملك فهد</td>
      <td>جنوب</td>
      <td>250</td>
      <td>0</td>
      <td>4</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>34000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1946</th>
      <td>الدمام</td>
      <td>حي ضاحية الملك فهد</td>
      <td>شمال</td>
      <td>250</td>
      <td>0</td>
      <td>5</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>30000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>601</th>
      <td>الرياض</td>
      <td>حي منفوحة الجديدة</td>
      <td>شرق</td>
      <td>200</td>
      <td>24</td>
      <td>5</td>
      <td>2</td>
      <td>2</td>
      <td>1</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>20000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>608</th>
      <td>الرياض</td>
      <td>حي بدر</td>
      <td>شرق</td>
      <td>250</td>
      <td>4</td>
      <td>3</td>
      <td>5</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>16000</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>934</th>
      <td>الرياض</td>
      <td>حي المونسية</td>
      <td>شرق</td>
      <td>250</td>
      <td>8</td>
      <td>4</td>
      <td>5</td>
      <td>2</td>
      <td>1</td>
      <td>1</td>
      <td>...</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>4500</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>80 rows × 24 columns</p>
</div>




```python
df['details'].fillna("",inplace= True)
```


```python
df['details'].isna().sum()
```




    0



# Data Exploration


```python
import seaborn as sns
import matplotlib.pyplot as plt
```


```python
sns.distplot(df['property_age'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\distributions.py:2619: FutureWarning: `distplot` is a deprecated function and will be removed in a future version. Please adapt your code to use either `displot` (a figure-level function with similar flexibility) or `histplot` (an axes-level function for histograms).
      warnings.warn(msg, FutureWarning)
    




    <AxesSubplot:xlabel='property_age', ylabel='Density'>




    
![png](images/output_18_2.png)
    



```python
sns.distplot(df['price'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\distributions.py:2619: FutureWarning: `distplot` is a deprecated function and will be removed in a future version. Please adapt your code to use either `displot` (a figure-level function with similar flexibility) or `histplot` (an axes-level function for histograms).
      warnings.warn(msg, FutureWarning)
    




    <AxesSubplot:xlabel='price', ylabel='Density'>




    
![png](images/output_19_2.png)
    



```python
sns.distplot(df['size'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\distributions.py:2619: FutureWarning: `distplot` is a deprecated function and will be removed in a future version. Please adapt your code to use either `displot` (a figure-level function with similar flexibility) or `histplot` (an axes-level function for histograms).
      warnings.warn(msg, FutureWarning)
    




    <AxesSubplot:xlabel='size', ylabel='Density'>




    
![png](images/output_20_2.png)
    



```python
sns.scatterplot(data=df,x = 'price', y = 'size')
```




    <AxesSubplot:xlabel='price', ylabel='size'>




    
![png](images/output_21_1.png)
    



```python
df = df[df['price'] < 500000]
df = df[df['size'] < 2000]
```


```python
sns.scatterplot(data=df,x = 'price', y = 'size')
```




    <AxesSubplot:xlabel='price', ylabel='size'>




    
![png](images/output_23_1.png)
    



```python
sns.distplot(df['price'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\distributions.py:2619: FutureWarning: `distplot` is a deprecated function and will be removed in a future version. Please adapt your code to use either `displot` (a figure-level function with similar flexibility) or `histplot` (an axes-level function for histograms).
      warnings.warn(msg, FutureWarning)
    




    <AxesSubplot:xlabel='price', ylabel='Density'>




    
![png](images/output_24_2.png)
    



```python
sns.distplot(df['size'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\distributions.py:2619: FutureWarning: `distplot` is a deprecated function and will be removed in a future version. Please adapt your code to use either `displot` (a figure-level function with similar flexibility) or `histplot` (an axes-level function for histograms).
      warnings.warn(msg, FutureWarning)
    




    <AxesSubplot:xlabel='size', ylabel='Density'>




    
![png](images/output_25_2.png)
    



```python
sns.barplot(data=df,x = 'price', y = 'city')
```




    <AxesSubplot:xlabel='price', ylabel='city'>




    
![png](images/output_26_1.png)
    



```python
df['city'].value_counts().plot.pie()
```




    <AxesSubplot:ylabel='city'>




    
![png](images/output_27_1.png)
    



```python
df['front'].value_counts(normalize=True).plot.barh()
```




    <AxesSubplot:>




    
![png](images/output_28_1.png)
    



```python
df['district'].value_counts()[:10]
```




       حي ضاحية الملك فهد     171
       حي اللؤلؤ              149
       حي التحلية             149
       حي الصواري             149
       حي العارض              135
       حي طيبة                124
       حي الرمال              117
       حي الروضة              109
       حي المحمدية            102
       حي المنار              100
    Name: district, dtype: int64




```python
sns.lineplot(data=df, y= 'price', x='city')
```




    <AxesSubplot:xlabel='city', ylabel='price'>




    
![png](images/output_30_1.png)
    



```python
sns.countplot(df['livingrooms'])
```

    C:\Users\user\anaconda3\lib\site-packages\seaborn\_decorators.py:36: FutureWarning: Pass the following variable as a keyword arg: x. From version 0.12, the only valid positional argument will be `data`, and passing other arguments without an explicit keyword will result in an error or misinterpretation.
      warnings.warn(
    




    <AxesSubplot:xlabel='livingrooms', ylabel='count'>




    
![png](images/output_31_2.png)
    



```python
df.bedrooms.value_counts().plot.pie()
```




    <AxesSubplot:ylabel='bedrooms'>




    
![png](images/output_32_1.png)
    



```python
import numpy as np
```


```python
def display_heatmap(df, label):
    mask = np.zeros_like(df.corr())
    triangle_indecies = np.triu_indices_from(mask)
    mask[triangle_indecies] = True
    mask[:5]

    plt.figure(figsize=(16,10))
    sns.set_style('white')
    sns.heatmap(df.corr(),mask=mask,annot=True,annot_kws={"size":14})
    plt.xticks(fontsize=14)
    plt.yticks(fontsize=14)
    plt.xlabel(label, fontsize=25)
    plt.show()
```


```python
display_heatmap(df, "All cities")
```


    
![png](images/output_35_0.png)
    



```python
cities = df['city'].value_counts().keys()

for city in cities:
    display_heatmap(df[df['city'] == city], city)
```


    
![png](images/output_36_0.png)
    



    
![png](images/output_36_1.png)
    



    
![png](images/output_36_2.png)
    



    
![png](images/output_36_3.png)
    



```python
sns.boxenplot(data = df, y = 'price', x='ac')
```




    <AxesSubplot:xlabel='ac', ylabel='price'>




    
![png](images/output_37_1.png)
    



```python
sns.boxenplot(data = df[df['city'] == ' الرياض'], y = 'price', x='ac')
```




    <AxesSubplot:xlabel='ac', ylabel='price'>




    
![png](images/output_38_1.png)
    



```python
sns.lineplot(data = df, y = 'price', x='size')
```




    <AxesSubplot:xlabel='size', ylabel='price'>




    
![png](images/output_39_1.png)
    



```python
sns.lineplot(data = df[df['city'] == ' الدمام'], y = 'price', x='size')
```




    <AxesSubplot:xlabel='size', ylabel='price'>




    
![png](images/output_40_1.png)
    



```python

```
